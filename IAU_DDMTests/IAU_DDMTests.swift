//
//  IAU_DDMTests.swift
//  IAU_DDMTests
//
//  Created by Luis Marcelino on 20/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import XCTest
@testable import IAU_DDM

class IAU_DDMTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPostalCode() {
        let json = [
            "adminCode2": "1009",
            "adminName3": "Leiria",
            "adminCode1": "13",
            "adminName2": "Leiria",
            "lng": -8.808557290012137,
            "countryCode": "PT",
            "postalCode": "2410-001",
            "adminName1": "Leiria",
            "placeName": "Andreus",
            "lat": 39.73747901500023
            ] as [String : Any]
        let postal = PostalCode.create(json: json)
        
        XCTAssertEqual(postal?.postalCode, "2410-001")
        XCTAssertEqual(postal?.placeName, "Andreus")
        
        let json2 = [
            "adminCode2": "1009",
            "adminName3": "Leiria",
            "adminCode1": "13",
            "adminName2": "Leiria",
            "lng": -8.808557290012137,
            "countryCode": "PT",
            "adminName1": "Leiria",
            "placeName": "Andreus",
            "lat": 39.73747901500023
            ] as [String : Any]
        XCTAssertNil (PostalCode.create(json: json2))
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testFlickrParse() {
        let json:[String:Any] = [
            "id":"30471182097",
            "owner":"163248236@N06",
            "secret":"6f84996042",
            "server":"1966",
            "farm":2,
            "title":"Intro Post: Casa Soria is Getting a Dining Room Update!",
            "ispublic":1,"isfriend":0,"isfamily":0
        ]
        let flickr = FlickrPhoto.create(fromJson: json)
        XCTAssertNotNil(flickr)
        XCTAssertEqual("30471182097", flickr?.id)
        XCTAssertEqual(2, flickr?.farm)
        XCTAssertEqual("1966", flickr?.server)
        XCTAssertEqual("6f84996042", flickr?.secret)
    }

}
