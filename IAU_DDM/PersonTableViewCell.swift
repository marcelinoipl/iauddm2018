//
//  PersonTableViewCell.swift
//  IAU_DDM
//
//  Created by Luis Marcelino on 28/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idadeLabel: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    
    func setAvatar(avatarName:String?) {
        guard let name = avatarName else {
            return
        }
        if let image = UIImage(named: name) {
            photoImage.image = image
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
