//
//  GeoNameClient.swift
//  IAU_DDM
//
//  Created by Luis Marcelino on 04/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

class GeoNamesClient {
    
    static func postalCodeSearch(postalCode: String, completionHandler: @escaping ([PostalCode]?, Error?)->Void ) {
        // Descarregar dados do servidor
        let url = URL(string: "http://api.geonames.org/postalCodeSearchJSON?country=pt&postalcode=\(postalCode)&maxRows=10&username=livro_ios5")!
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completionHandler(nil, error)
                return
            }
//            guard let httpResponse = response as? HTTPURLResponse,
//                (200...299).contains(httpResponse.statusCode) else {
//                    self.handleServerError(response)
//                    return
//            }
            
            // Processar dados -> Postal codes
            var postalCodes = [PostalCode]()
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:Any] {
                    let postalCodesJson = json["postalCodes"] as! [[String:Any]]
                    for postalCodeJson in postalCodesJson {
                        let pc = PostalCode.create(json: postalCodeJson)
                        if let postal = pc {
                            postalCodes.append(postal)
                        }
                    }
                }
            }
            // devolver dados (completion)
            completionHandler(postalCodes, nil)
        }
        task.resume()
    }
}
