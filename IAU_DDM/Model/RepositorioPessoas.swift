//
//  RepositorioPessoas.swift
//  IAU_DDM
//
//  Created by Catarina Silva on 19/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

class RepositorioPessoas {
    
    static let repositorio = RepositorioPessoas()
    
    private init () {}
    
    var pessoas = [Pessoa]()
    let defaults = UserDefaults.standard

    func obterPessoas () {
        if let d = defaults.data(forKey: Constants.PEOPLE_ARCHIVE) {
            pessoas = loadPersonArray(unarchivedObject: d)
        }
    }
    
    func guardarPessoas () {
        let d = archivePersonArray(peopleDataArray: pessoas)
        if d != nil {
            defaults.set(d, forKey: Constants.PEOPLE_ARCHIVE)
            
            defaults.synchronize()
        }
    }
    
    func archivePersonArray(peopleDataArray : [Pessoa]) -> Data? {
        return try? NSKeyedArchiver.archivedData(withRootObject: peopleDataArray as Array, requiringSecureCoding: false)
    }
    func loadPersonArray(unarchivedObject: Data) -> [Pessoa] {
        return try! NSKeyedUnarchiver.unarchivedObject(ofClasses: [NSArray.self, Pessoa.self], from: unarchivedObject) as! [Pessoa]
    }
}
