//
//  File.swift
//  IAU_DDM
//
//  Created by Luis Marcelino on 04/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

struct PostalCode {
    let postalCode:String
    let placeName:String
    let lng:Double
    let lat: Double
    
    static func create(json: [String:Any]) -> PostalCode? {
        guard
            let code = json["postalCode"] as? String,
            let name =  json["placeName"] as? String,
            let lng = json["lng"] as? Double,
            let lat = json["lat"] as? Double
            else {
                return nil
        }
        return PostalCode(
            postalCode: code,
            placeName: name,
            lng: lng,
            lat: lat
        )
    }
}
