//
//  Constants.swift
//  IAU_DDM
//
//  Created by Catarina Silva on 19/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

struct Constants {
    static let INT_VALUE = "intValue"
    static let NOME = "name"
    static let EMAIL = "email"
    static let IDADE = "age"
    static let AVATAR = "photo"
    static let POSTALCODE = "postalcode"
    static let PLACE_NAME = "placeName"
    static let LATITUDE = "latitude"
    static let LONGITUDE = "longitude"
    static let PEOPLE_ARCHIVE = "people_archive"
}
