//
//  Pessoa.swift
//  IAU_DDM
//
//  Created by Catarina Silva on 21/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

class Pessoa: NSObject, NSSecureCoding {
    static var supportsSecureCoding: Bool {
        return true
    }
    
    var nome:String
    var email:String
    var idade = 18
    var avatar:String?
    var postalCode: PostalCode?
    
    init(nome:String, email:String, idade:Int) {
        self.nome = nome
        self.email = email
        self.idade = idade
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(nome, forKey: Constants.NOME)
        aCoder.encode(email, forKey: Constants.EMAIL)
        aCoder.encode(avatar, forKey: Constants.AVATAR)
        aCoder.encode(idade, forKey: Constants.IDADE)
        //proteger  as 2 linhas seguintes com if let
        aCoder.encode(postalCode?.postalCode, forKey: Constants.POSTALCODE)
        aCoder.encode(postalCode?.placeName, forKey: Constants.PLACE_NAME)
        
        if let lat = postalCode?.lat {
            aCoder.encode(lat, forKey: Constants.LATITUDE)
        }
        if let lon = postalCode?.lng {
            aCoder.encode(lon, forKey: Constants.LONGITUDE)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        nome = aDecoder.decodeObject(forKey: Constants.NOME) as! String
        email = aDecoder.decodeObject(forKey: Constants.EMAIL) as! String
        avatar = aDecoder.decodeObject(forKey: Constants.AVATAR) as? String
        idade = aDecoder.decodeInteger(forKey: Constants.IDADE)
        
        if let pc = aDecoder.decodeObject(forKey: Constants.POSTALCODE) {
            let p = PostalCode(
                postalCode: pc as! String,
                placeName: aDecoder.decodeObject(forKey: Constants.PLACE_NAME) as! String,
                lng: aDecoder.decodeDouble(forKey: Constants.LONGITUDE),
                lat: aDecoder.decodeDouble(forKey: Constants.LATITUDE))
            self.postalCode = p
        }
    }
}

