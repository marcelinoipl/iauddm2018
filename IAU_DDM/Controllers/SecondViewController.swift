//
//  SecondViewController.swift
//  IAU_DDM
//
//  Created by Luis Marcelino on 20/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var pessoa: Pessoa?
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let p = pessoa {
            print (p.nome)
            nameLabel.text = p.nome
            emailLabel.text = p.email
            ageLabel.text = "\(p.idade)"
        } else {
            print("Não foram passados dados")
        }
    }
}

