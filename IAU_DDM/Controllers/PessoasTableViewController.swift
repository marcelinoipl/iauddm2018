//
//  PessoasTableViewController.swift
//  IAU_DDM
//
//  Created by Catarina Silva on 27/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit
import Social


class PessoasTableViewController: UITableViewController {

    //let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let defaults = UserDefaults.standard
        //let x = 2
        
        //defaults.set(x, forKey: Constants.INT_VALUE)
       
        let y = defaults.integer(forKey: Constants.INT_VALUE)
        print(y)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return RepositorioPessoas.repositorio.pessoas.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celulaPessoa", for: indexPath) as! PersonTableViewCell

        // Configure the cell...
        let person = RepositorioPessoas.repositorio.pessoas[indexPath.row]
        cell.nameLabel?.text = person.nome
        //cell.detailTextLabel?.text = appDelegate.pessoas[indexPath.row].email
        if let postalCode = person.postalCode {
            cell.idadeLabel?.text = postalCode.placeName
        }
        else {
            cell.idadeLabel?.text = "\(person.idade) anos"
        }
        
        cell.setAvatar(avatarName: person.avatar)

        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueDetalhePessoa", sender: RepositorioPessoas.repositorio.pessoas[indexPath.row])
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    //MARK: - Sharing
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
        let activityViewController = UIActivityViewController(activityItems: ["TESTE","OLÁ"], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        //validar SEGUE
        //validar Destination VC
        //validar sender
        if segue.identifier == "segueDetalhePessoa" {
            if let destinationVC = segue.destination as? SecondViewController {
                if let pessoa = sender as? Pessoa {
                    destinationVC.pessoa = pessoa
                }
            }
        }
    }
    

}
