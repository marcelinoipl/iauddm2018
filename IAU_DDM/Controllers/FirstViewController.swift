//
//  FirstViewController.swift
//  IAU_DDM
//
//  Created by Luis Marcelino on 20/09/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var campoPessoa: UITextField!
    
    @IBOutlet weak var campoEmail: UITextField!
    
    @IBOutlet weak var campoIdade: UITextField!

    @IBOutlet weak var postalCodeField: UITextField!

    @IBOutlet weak var campoAvatar: UITextField!
    
    @IBOutlet weak var photoImage: UIImageView!
    
    var person = Pessoa(nome: "", email: "", idade: 0)
    
    //let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myLabel.text = "Olaaaaa!"
        
        //        let i = 10
        //        print ("i: \(i)")
        //
        //        var j:Int?
        //        print ("j: \(j)")
        //        j = 20
        //        print ("initialized j: \(j)")
        //
        //        if let unwrappedJ = j {
        //            print ("unwrappedJ: \(unwrappedJ)")
        //        }
        //        print("confident j: \(j!)")
    }
    
    func showAlert(message:String, title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //IBActions
    
    @IBAction func clickAction(_ sender: Any) {
        myLabel.text = "Foste!"
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func guardarPessoa(_ sender: Any) {
        
     //NOVO TPC corrigir unwarpp para != ""
        guard let n = campoPessoa.text else {
            return
        }
        // neste ponto n existe
        if let e = campoEmail.text {
            if let a = campoIdade.text {
                if let integerAge = Int(a) {
//                    let pessoa = Pessoa(nome: n, email: e, idade: integerAge)
                    person.nome = n
                    person.email = e
                    person.idade = integerAge
                    
                    person.avatar = campoAvatar.text
                    RepositorioPessoas.repositorio.pessoas.append(person)

//                    showAlert(message: "Pessoa guardada!", title: "")
                    self.dismiss(animated: true, completion: nil)
                } else {
                    showAlert(message: "Todos os campos têm de estar corretos.", title: "")
                }
            }
            campoPessoa.text = ""
            campoEmail.text = ""
            campoIdade.text = ""
        }
        RepositorioPessoas.repositorio.guardarPessoas()
    }
    
    
    @IBAction func listarPessoas(_ sender: UIButton) {
        for p in RepositorioPessoas.repositorio.pessoas {
            print("nome:\(p.nome) email:\(p.email) idade: \(p.idade)")
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == campoPessoa {
            campoEmail.becomeFirstResponder()
            return false
        }
        if textField == postalCodeField {
            GeoNamesClient.postalCodeSearch(postalCode: postalCodeField.text!) { (postalCodes, error) in
                OperationQueue.main.addOperation {
                    if error != nil {
                        let alert = UIAlertController(title: "Postal code error", message: error!.localizedDescription, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                            NSLog("The \"OK\" alert occured.")
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else {
                        let controler = PostalCodePickerTableViewController()
                        controler.postalCodes = postalCodes!
                        controler.person = self.person
                        self.navigationController?.pushViewController(controler, animated: true)
                    }
                }
                
            }
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func takePhotoAction(_ sender: Any) {
        let controller = UIImagePickerController()
        controller.sourceType = .photoLibrary
        controller.delegate = self
        
        self.present(controller, animated: true, completion: nil)
    }
    
    // #MARK --- UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        photoImage.image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
    }
}

