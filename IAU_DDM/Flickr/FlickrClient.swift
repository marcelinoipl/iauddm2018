//
//  FlickrClient.swift
//  IAU_DDM
//
//  Created by Luis Marcelino on 18/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

class FlickrClient {
    
    static func fetchPhotos (completionHandler: @escaping ([FlickrPhoto]?, Error?) -> Void) {
        let url = URL(string: "https://www.flickr.com/services/rest/?api_key=f9333373bf926daaef986f5251554e9e&format=json&method=flickr.photos.getRecent")!
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let e = err {
                completionHandler(nil, e)
                return
            }
            do {
                var jsonString = String(data: data!, encoding: .utf8)!
                
                let startIndex = jsonString.index(jsonString.startIndex, offsetBy: 14)
                jsonString = String(jsonString[startIndex...])
                let endIndex = jsonString.index(jsonString.endIndex, offsetBy: -1)
                jsonString = String(jsonString[..<endIndex])
                let data1 = jsonString.data(using: .utf8)!
                
                let json = try JSONSerialization.jsonObject(with: data1, options: .allowFragments) as! [String:Any]
                
                let photosParentJson = json["photos"] as! [String:Any]
                if let photosJson = photosParentJson["photo"] as? [[String:Any]] {
                    var photos = [FlickrPhoto]()
                    for photoJson in photosJson {
                        if let p = FlickrPhoto.create(fromJson: photoJson) {
                            photos.append(p)
                        }
                    }
                    completionHandler(photos, nil)
                }
            }
            catch {
                completionHandler(nil, error)
            }            
        }.resume()
    }
}
