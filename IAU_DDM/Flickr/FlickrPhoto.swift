//
//  FlickrPhoto.swift
//  IAU_DDM
//
//  Created by Luis Marcelino on 18/10/2018.
//  Copyright © 2018 Luis Marcelino. All rights reserved.
//

import Foundation

struct FlickrPhoto {
    let id:String
    let secret:String
    let server:String
    let farm:Int
    let title:String
    let isPublic:Int
    
    static func create(fromJson json:[String:Any]) -> FlickrPhoto? {
        guard let id = json["id"] as? String else {
            return nil
        }
        guard let secret = json["secret"] as? String else {
            return nil
        }
        guard let server = json["server"] as? String else {
            return nil
        }
        guard let farm = json["farm"] as? Int else {
            return nil
        }
        guard let title = json["title"] as? String else {
            return nil
        }
        guard let isPublic = json["ispublic"] as? Int else {
            return nil
        }
        return FlickrPhoto(id: id, secret: secret, server: server, farm: farm, title: title, isPublic: isPublic)
    }
}
